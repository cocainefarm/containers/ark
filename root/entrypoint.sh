#!/usr/bin/env bash
echo "###########################################################################"
echo "# Ark Server - " `date`
echo "###########################################################################"

export TERM=linux

envsubst < /etc/arkmanager/arkmanager.cfg.template > /etc/arkmanager/arkmanager.cfg
envsubst < /etc/arkmanager/instances/main.cfg.template > /etc/arkmanager/instances/main.cfg

mkdir -p /data/log

function stop {
	if [ -z "${ARK_WARN_ON_STOP}" ];then 
		arkmanager stop --warn
	else
		arkmanager stop
	fi
	exit
}

if [ ! -d "${ARK_SERVER_ROOT}"/ShooterGame  ] || [ ! -f "${ARK_SERVER_ROOT}"/PackageInfo.bin ]; then 
	arkmanager install
fi

arkmanager start --no-background
